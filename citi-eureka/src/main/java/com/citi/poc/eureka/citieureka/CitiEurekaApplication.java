package com.citi.poc.eureka.citieureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class CitiEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitiEurekaApplication.class, args);
	}
}
